#! /bin/bash
set -ex
bundle exec jekyll clean

for i in {1..5}
do
   bundle exec jekyll build
   sleep 2
done
