---
layout: post
title: 'Για τον Υφυπουργό Παιδείας'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/gia-ton-yfypoyrgo-paideias.pdf'
pdf_name: '1979 - Για τον Υφυπουργό Παιδείας.pdf'
year: '1979'
---
