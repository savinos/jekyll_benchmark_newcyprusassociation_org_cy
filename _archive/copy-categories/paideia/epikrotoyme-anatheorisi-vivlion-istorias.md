---
layout: post
title: 'Επικροτούμε (Αναθεώρηση Βιβλίων Ιστορίας)'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/epikrotoyme-anatheorisi-vivlion-istorias.pdf'
pdf_name: '2007 - Επικροτούμε (Αναθεώρηση Βιβλίων Ιστορίας).pdf'
year: '2007'
---
