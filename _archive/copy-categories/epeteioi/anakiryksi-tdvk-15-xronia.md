---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (15 χρόνια)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-15-xronia.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (15 χρόνια) - 1998.pdf'
year: '1998'
---
