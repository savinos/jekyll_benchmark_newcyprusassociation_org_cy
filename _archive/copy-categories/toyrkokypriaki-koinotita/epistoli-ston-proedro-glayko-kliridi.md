---
layout: post
title: 'Επιστολή στον πρόεδρο Γλαύκο Κληρίδη'
categories:
  - 'toyrkokypriaki-koinotita'
tags:
  - 'Τουρκοκυπριακή Κοινότητα'
pdf_path: './assets/archive/toyrkokypriaki-koinotita/epistoli-ston-proedro-glayko-kliridi.pdf'
pdf_name: '1998 - Επιστολή στον πρόεδρο Γλαύκο Κληρίδη.pdf'
year: '1998'
---
