---
layout: post
title: 'Προσέλκυση ΤΚ στις ελεύθερες περιοχές'
categories:
  - 'toyrkokypriaki-koinotita'
tags:
  - 'Τουρκοκυπριακή Κοινότητα'
pdf_path: './assets/archive/toyrkokypriaki-koinotita/proselkysi-tk-stis-eleytheres-perioxes.pdf'
pdf_name: '1998 - Προσέλκυση ΤΚ στις ελεύθερες περιοχές.pdf'
year: '1998'
---
