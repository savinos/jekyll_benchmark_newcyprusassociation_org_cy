---
layout: post
title: 'Προκαταρκτική έκθεση Παροχή βοήθειας στους Τουρκοκυπρίους'
categories:
  - 'toyrkokypriaki-koinotita'
tags:
  - 'Τουρκοκυπριακή Κοινότητα'
pdf_path: './assets/archive/toyrkokypriaki-koinotita/prokatarktiki-ekthesi-paroxi-voitheias-stoys-toyrkokyprioys.pdf'
pdf_name: '1975 - Προκαταρκτική έκθεση - Παροχή βοήθειας στους Τουρκοκυπρίους.pdf'
year: '1975'
---
