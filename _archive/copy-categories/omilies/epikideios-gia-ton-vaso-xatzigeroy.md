---
layout: post
title: 'Επικήδειος για τον Βάσο Χατζηγέρου'
categories:
  - 'omilies'
tags:
  - 'Ομιλίες'
pdf_path: './assets/archive/omilies/epikideios-gia-ton-vaso-xatzigeroy.pdf'
pdf_name: '2012 - Επικήδειος για τον Βάσο Χατζηγέρου.pdf'
year: '2012'
---
