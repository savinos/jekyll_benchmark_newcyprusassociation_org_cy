---
layout: post
title: 'Παρουσίαση ΝΚΣ στο Τεχνολογικό Ινστιτούτο'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/peri-syndesmoy/paroysiasi-nks-sto-texnologiko-institoyto.pdf'
pdf_name: '1977 - Παρουσίαση ΝΚΣ στο Τεχνολογικό Ινστιτούτο.pdf'
year: '1977'
---
