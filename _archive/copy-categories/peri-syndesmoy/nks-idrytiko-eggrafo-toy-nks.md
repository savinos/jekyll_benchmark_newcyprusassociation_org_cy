---
layout: post
title: 'ΝΚΣ Ιδρυτικό Έγραφο του ΝΚΣ'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/peri-syndesmoy/nks-idrytiko-eggrafo-toy-nks.pdf'
pdf_name: 'ΝΚΣ - Ιδρυτικό Έγγραφο του ΝΚΣ - 1974.pdf'
year: '1974'
---
