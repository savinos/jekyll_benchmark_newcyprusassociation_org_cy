---
layout: post
title: 'Πρόσκληση ΝΚΣ από Ντεκτάς'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/prosklisi-nks-apo-ntektas.pdf'
pdf_name: '1976 - Πρόσκληση ΝΚΣ από Ντεκτάς.pdf'
year: '1976'
---
