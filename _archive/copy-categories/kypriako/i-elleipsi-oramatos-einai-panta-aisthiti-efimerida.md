---
layout: post
title: 'Η έλειψη οράματος είναι πάντα αισθητή (Εφημερίδα)'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/i-elleipsi-oramatos-einai-panta-aisthiti-efimerida.pdf'
pdf_name: '1999 - Η έλλειψη οράματος είναι πάντα αισθητή (Εφημερίδα).pdf'
year: '1999'
---
