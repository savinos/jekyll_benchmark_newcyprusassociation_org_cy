---
layout: post
title: '25 χρόνια από την ιδρυση του ΝΚΣ'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/en/peri-syndesmoy/25-xronia-apo-tin-idrysi-toy-nks.pdf'
pdf_name: '25 χρόνια από την ιδρυση του ΝΚΣ - EN - 2000.pdf'
year: '2000'
language: 'Αγγλικά'
---
