---
layout: post
title: 'ΝΚΣ Διακήρυξη'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/en/peri-syndesmoy/nks-diakiryksi.pdf'
pdf_name: 'ΝΚΣ - Διακήρυξη - EN.pdf'
language: 'Αγγλικά'
---
