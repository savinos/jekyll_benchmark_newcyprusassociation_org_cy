---
layout: post
title: 'Ambasador of Cuba'
categories:
  - 'diethni'
tags:
  - 'Διεθνή'
pdf_path: './assets/archive/en/diethni/ambassador-of-cuba.pdf'
pdf_name: '1976 - Ambassador of Cuba.pdf'
year: '1976'
language: 'Αγγλικά'
---
