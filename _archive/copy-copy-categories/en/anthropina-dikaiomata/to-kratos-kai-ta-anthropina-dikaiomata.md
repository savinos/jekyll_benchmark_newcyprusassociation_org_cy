---
layout: post
title: 'Το κράτος και τα ανθρώπινα δικαιώματα'
categories:
  - 'anthropina-dikaiomata'
tags:
  - 'Ανθρώπινα Δικαιώματα'
pdf_path: './assets/archive/en/anthropina-dikaiomata/to-kratos-kai-ta-anthropina-dikaiomata.pdf'
pdf_name: '2004 - Το κράτος και τα ανθρώπινα δικαιώματα - EN.pdf'
year: '2004'
language: 'Αγγλικά'
---
