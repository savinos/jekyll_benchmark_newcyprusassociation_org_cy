---
layout: post
title: 'Δηλώσεις Μακαρίου'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/diloseis-makarioy.pdf'
pdf_name: '1976 - Δηλώσεις Μακαρίου.pdf'
year: '1976'
---
