---
layout: post
title: 'Αγώνας με βάση την πραγματικότητα και όχι ψευδαισθήσεις'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/agonas-me-vasi-tin-pragmatikotita-kai-oxi-pseydaisthiseis.pdf'
pdf_name: '1999 - Αγώνας με βάση την πραγματικότητα και όχι ψευδαισθήσεις.pdf'
year: '1999'
---
