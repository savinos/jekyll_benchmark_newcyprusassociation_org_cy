---
layout: post
title: 'Μεταφορά ΤΚ στα κατεχόμενα'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/metafora-tk-sta-katexomena.pdf'
pdf_name: '1975 - Μεταφορά ΤΚ στα κατεχόμενα.pdf'
year: '1975'
---
