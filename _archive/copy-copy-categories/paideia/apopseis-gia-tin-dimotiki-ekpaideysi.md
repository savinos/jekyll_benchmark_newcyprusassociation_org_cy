---
layout: post
title: 'Απόψεις για την Δημοτική Εκπαίδευση'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/apopseis-gia-tin-dimotiki-ekpaideysi.pdf'
pdf_name: '1999 - Απόψεις για την Δημοτική Εκπαίδευση.pdf'
year: '1999'
---
