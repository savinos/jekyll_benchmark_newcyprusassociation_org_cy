---
layout: post
title: 'Δημοσίευμα εφημερίδας Σημερινή'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/peri-syndesmoy/dimosieyma-efimeridas-simerini.pdf'
pdf_name: '1987 - Δημοσίευμα εφημερίδας Σημερινή.pdf'
year: '1987'
---
