---
layout: post
title: '20 χρόνια από την ιδρυση του ΝΚΣ'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/peri-syndesmoy/20-xronia-apo-tin-idrysi-toy-nks.pdf'
pdf_name: '20 χρόνια από την ιδρυση του ΝΚΣ - 1995.pdf'
year: '1995'
---
