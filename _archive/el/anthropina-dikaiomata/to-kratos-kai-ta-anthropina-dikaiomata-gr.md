---
layout: post
title: 'Το κράτος και τα ανθρώπινα δικαιώματα GR'
categories:
  - 'anthropina-dikaiomata'
tags:
  - 'Ανθρώπινα Δικαιώματα'
pdf_path: './assets/archive/el/anthropina-dikaiomata/to-kratos-kai-ta-anthropina-dikaiomata-gr.pdf'
pdf_name: '2004 - Το κράτος και τα ανθρώπινα δικαιώματα - GR.pdf'
year: '2004'
language: 'Ελληνικά'
---
