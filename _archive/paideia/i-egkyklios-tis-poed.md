---
layout: post
title: 'Η Εγκύκλιος της ΠΟΕΔ'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/i-egkyklios-tis-poed.pdf'
pdf_name: '2009 - Η Εγκύκλιος της ΠΟΕΔ.pdf'
year: '2009'
---
