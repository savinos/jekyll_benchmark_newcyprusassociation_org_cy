---
layout: post
title: 'Για την Διαφώτιση στο οδόφραγμα του Λήδρα Πάλας2'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/gia-tin-diafotisi-sto-odofragma-toy-lidra-palas2.pdf'
pdf_name: '1998 - Για την Διαφώτιση στο οδόφραγμα του Λήδρα Πάλας-2.pdf'
year: '1998'
---
