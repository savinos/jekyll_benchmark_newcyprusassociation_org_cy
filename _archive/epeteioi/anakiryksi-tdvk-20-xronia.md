---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (20 χρόνια)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-20-xronia.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (20 χρόνια) - 2003.pdf'
year: '2003'
---
